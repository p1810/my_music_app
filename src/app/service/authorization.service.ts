import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenResponse } from '../interface/TokenResponse';
import { UsersProfile } from '../interface/UsersProfile';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private router: Router, private http: HttpClient) { }

  startSession() {
    var url = `${environment.authorizeUrlSpotify}${environment.authorizeCodeSpotify}`;
    url += '?response_type=code';
    url += `&scope=${environment.scope.join(',')}`;
    url += `&client_id=${environment.client_id}`;
    url += `&redirect_uri=${environment.redirect_uri}`;

    window.location.href = url;
  }

  getToken(code: string): Observable<TokenResponse> {
    var reqHeader = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var reqBody = `grant_type=authorization_code&`;
    reqBody += `client_id=${environment.client_id}&`;
    reqBody += `client_secret=${environment.client_secret}&`;
    reqBody += `code=${code}&`;
    reqBody += `redirect_uri=${environment.redirect_uri}`;

    return this.http.post<TokenResponse>(`${environment.authorizeUrlSpotify}${environment.authorizeTokenSpotify}`, reqBody, { headers: reqHeader });
  }

  finishSession() {
    localStorage.removeItem('spotifyTokenAccess');
    this.router.navigateByUrl('/login');
  }

  getUser(): Observable<UsersProfile> {
    var reqHeader = new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('spotifyTokenAccess')
    });
    return this.http.get<UsersProfile>(`${environment.apiUrlSpotify}/v1/me`, { headers: reqHeader });
  }
}
