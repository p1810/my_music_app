import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ListTrack } from '../interface/ListTrack';
import { Favorite } from '../interface/Favorite';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http: HttpClient) { }

  getTracks(): Observable<ListTrack>{
    var reqHeader = new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('spotifyTokenAccess')
    });
    return this.http.get<ListTrack>(`${environment.apiUrlSpotify}/v1/browse/new-releases`, { headers: reqHeader });
  }

  getFavorites(): Observable<Favorite>{
    var reqHeader = new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('spotifyTokenAccess')
    });
    return this.http.get<Favorite>(`${environment.apiUrlSpotify}/v1/me/tracks?limit=50&offset=0&market=from_token`, { headers: reqHeader });
  }

}
