import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PgAuthorizationComponent } from './component-atomic/pages/pg-authorization/pg-authorization.component';
import { PgFavoriteComponent } from './component-atomic/pages/pg-favorite/pg-favorite.component';
import { PgHomeComponent } from './component-atomic/pages/pg-home/pg-home.component';
import { PgLoginComponent } from './component-atomic/pages/pg-login/pg-login.component';

const routes: Routes = [
  {path: '', component: PgHomeComponent},
  {path: 'favorito', component: PgFavoriteComponent},
  {path: 'login', component: PgLoginComponent},
  {path: 'autorizacion', component: PgAuthorizationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
