import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PgFavoriteComponent } from './pg-favorite.component';

describe('PgFavoriteComponent', () => {
  let component: PgFavoriteComponent;
  let fixture: ComponentFixture<PgFavoriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PgFavoriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PgFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
