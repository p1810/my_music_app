import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/service/authorization.service';

@Component({
  selector: 'app-pg-login',
  templateUrl: './pg-login.component.html',
  styleUrls: ['./pg-login.component.scss']
})

export class PgLoginComponent implements OnInit {

  login: any;
  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
    this.authorizationService.getUser()
      .subscribe(data => this.login = data);
  }
}

