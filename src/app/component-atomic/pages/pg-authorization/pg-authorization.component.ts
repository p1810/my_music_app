import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationService } from 'src/app/service/authorization.service';

@Component({
  selector: 'app-pg-authorization',
  templateUrl: './pg-authorization.component.html',
  styleUrls: ['./pg-authorization.component.scss']
})
export class PgAuthorizationComponent implements OnInit {

  constructor(private router: Router, private routeActive: ActivatedRoute, private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
    //si tiene code en la ruta entonces cambia el code por un token en spotify
    this.routeActive.queryParams
      .subscribe(params => {
        var code = params.code;
        var error = params.error;

        if (code != null) {
          this.authorizationService.getToken(code).subscribe(response => {
            if (response != null) {
              if (response.access_token != null) {
                localStorage.setItem('spotifyTokenAccess', response.access_token);
                this.router.navigateByUrl('');
              }
            }
          });
        } else if (error != null) {
          this.router.navigateByUrl('/login');
        }
      });
  }

}
