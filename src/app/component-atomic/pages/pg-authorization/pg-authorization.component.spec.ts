import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PgAuthorizationComponent } from './pg-authorization.component';

describe('PgAuthorizationComponent', () => {
  let component: PgAuthorizationComponent;
  let fixture: ComponentFixture<PgAuthorizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PgAuthorizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PgAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
