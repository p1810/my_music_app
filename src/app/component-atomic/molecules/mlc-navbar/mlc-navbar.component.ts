import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/service/authorization.service';

@Component({
  selector: 'app-mlc-navbar',
  templateUrl: './mlc-navbar.component.html',
  styleUrls: ['./mlc-navbar.component.scss']
})
export class MlcNavbarComponent implements OnInit {
  
  userName!: string;
  constructor(private route: Router, private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
    this.authorizationService.getUser()
      .subscribe(
        response => {
          this.userName = response.display_name;
        },
        error => {
          this.route.navigateByUrl('/login');
        });
  }

  navResponsive() {
    var x = document.getElementById("myTopnav");
    if (x != null) {
      if (x.className === "topnav") {
          x.className += " responsive";
      } else {
          x.className = "topnav";
      }
    }
  }

}
