import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlcNavbarComponent } from './mlc-navbar.component';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe('MlcNavbarComponent', () => {
  let component: MlcNavbarComponent;
  let fixture: ComponentFixture<MlcNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlcNavbarComponent ],
      imports: [RouterTestingModule, HttpClientTestingModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlcNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('se creo correctamente', () => {
    expect(component).toBeTruthy();
  });
});
