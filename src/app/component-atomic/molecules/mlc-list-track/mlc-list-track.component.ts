import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MusicService } from 'src/app/service/music.service';

@Component({
  selector: 'app-mlc-list-track',
  templateUrl: './mlc-list-track.component.html',
  styleUrls: ['./mlc-list-track.component.scss']
})
export class MlcListTrackComponent implements OnInit {

  musicList: any;
  constructor(private route: Router, private musicService: MusicService) { }

  ngOnInit(): void {
    this.musicService.getTracks()
      .subscribe(
        response => {
          this.musicList = response.albums.items;
        },
        error => {
          this.route.navigateByUrl('/login');
        });
  }

}

