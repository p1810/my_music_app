import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MlcListTrackComponent } from './mlc-list-track.component';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";

fdescribe('MlcListTrackComponent', () => {
  let component: MlcListTrackComponent;
  let fixture: ComponentFixture<MlcListTrackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlcListTrackComponent ],
      imports: [RouterTestingModule, HttpClientTestingModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlcListTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('se creo correctamente', () => {
    expect(component).toBeTruthy();
  });
});
