import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MusicService } from 'src/app/service/music.service';

@Component({
  selector: 'app-mlc-list-favorite',
  templateUrl: './mlc-list-favorite.component.html',
  styleUrls: ['./mlc-list-favorite.component.scss']
})
export class MlcListFavoriteComponent implements OnInit {

  musicList: any;
  constructor(private route: Router, private musicService: MusicService) { }

  ngOnInit(): void {
    this.musicService.getFavorites()
      .subscribe(
        response => {
          this.musicList = response.items;
        },
        error => {
          this.route.navigateByUrl('/login');
        });
  }
}
