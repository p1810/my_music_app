import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MlcListFavoriteComponent } from './mlc-list-favorite.component';

describe('MlcListFavoriteComponent', () => {
  let component: MlcListFavoriteComponent;
  let fixture: ComponentFixture<MlcListFavoriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlcListFavoriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlcListFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
