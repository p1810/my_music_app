import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-atm-card',
    templateUrl: './atm-card.component.html',
    styleUrls: ['./atm-card.component.scss'],
})
export class AtmCardComponent implements OnInit {
    @Input() image: string = '';
    @Input() textName: string = '';
    @Input() textArtists: string = '';

    constructor() {}

    ngOnInit(): void {}
}
