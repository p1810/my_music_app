import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/service/authorization.service';

@Component({
  selector: 'app-atm-btn-login',
  templateUrl: './atm-btn-login.component.html',
  styleUrls: ['./atm-btn-login.component.scss']
})
export class AtmBtnLoginComponent implements OnInit {

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
  }

  startSession() {
    this.authorizationService.startSession();
  }

}
