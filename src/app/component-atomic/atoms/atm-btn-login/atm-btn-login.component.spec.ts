import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmBtnLoginComponent } from './atm-btn-login.component';

describe('AtmBtnLoginComponent', () => {
  let component: AtmBtnLoginComponent;
  let fixture: ComponentFixture<AtmBtnLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmBtnLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmBtnLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
