import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmBtnFavoriteComponent } from './atm-btn-favorite.component';

describe('AtmBtnFavoriteComponent', () => {
  let component: AtmBtnFavoriteComponent;
  let fixture: ComponentFixture<AtmBtnFavoriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmBtnFavoriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmBtnFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
