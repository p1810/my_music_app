import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmBtnLogoutComponent } from './atm-btn-logout.component';

describe('AtmBtnLogoutComponent', () => {
  let component: AtmBtnLogoutComponent;
  let fixture: ComponentFixture<AtmBtnLogoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmBtnLogoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmBtnLogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
