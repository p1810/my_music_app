import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/service/authorization.service';

@Component({
  selector: 'app-atm-btn-logout',
  templateUrl: './atm-btn-logout.component.html',
  styleUrls: ['./atm-btn-logout.component.scss']
})
export class AtmBtnLogoutComponent implements OnInit {

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
  }

  logout() {
    this.authorizationService.finishSession();
  }

}
