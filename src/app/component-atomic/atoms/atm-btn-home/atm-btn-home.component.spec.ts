import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmBtnHomeComponent } from './atm-btn-home.component';

describe('AtmBtnHomeComponent', () => {
  let component: AtmBtnHomeComponent;
  let fixture: ComponentFixture<AtmBtnHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmBtnHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmBtnHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
