import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-atm-btn-home',
  templateUrl: './atm-btn-home.component.html',
  styleUrls: ['./atm-btn-home.component.scss']
})
export class AtmBtnHomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goHome() {
    this.router.navigateByUrl('');
  }

}
