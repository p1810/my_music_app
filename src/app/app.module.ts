import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MlcNavbarComponent } from './component-atomic/molecules/mlc-navbar/mlc-navbar.component';
import { AtmBtnFavoriteComponent } from './component-atomic/atoms/atm-btn-favorite/atm-btn-favorite.component';
import { AtmBtnLogoutComponent } from './component-atomic/atoms/atm-btn-logout/atm-btn-logout.component';
import { AtmBtnHomeComponent } from './component-atomic/atoms/atm-btn-home/atm-btn-home.component';
import { AtmBtnLoginComponent } from './component-atomic/atoms/atm-btn-login/atm-btn-login.component';
import { MlcListTrackComponent } from './component-atomic/molecules/mlc-list-track/mlc-list-track.component';
import { MlcListFavoriteComponent } from './component-atomic/molecules/mlc-list-favorite/mlc-list-favorite.component';
import { PgHomeComponent } from './component-atomic/pages/pg-home/pg-home.component';
import { PgFavoriteComponent } from './component-atomic/pages/pg-favorite/pg-favorite.component';
import { PgLoginComponent } from './component-atomic/pages/pg-login/pg-login.component';
import { HttpClientModule } from '@angular/common/http';
import { PgAuthorizationComponent } from './component-atomic/pages/pg-authorization/pg-authorization.component';
import { AtmCardComponent } from './component-atomic/atoms/atm-card/atm-card.component';

@NgModule({
  declarations: [
    AppComponent,
    MlcNavbarComponent,
    AtmBtnFavoriteComponent,
    AtmBtnLogoutComponent,
    AtmBtnHomeComponent,
    AtmBtnLoginComponent,
    MlcListTrackComponent,
    MlcListFavoriteComponent,
    PgHomeComponent,
    PgFavoriteComponent,
    PgLoginComponent,
    PgAuthorizationComponent,
    AtmCardComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
