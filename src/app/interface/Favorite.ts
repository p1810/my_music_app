export interface Favorite {
    items: Item[]
}

interface Item {
    track: Track
}

interface Track {
    id: string;
    name: string;
    popularity: string;
    album: Album;
    artists: [Artist];
}

interface Album {
    name: string;
    release_date: string;
    images: [Image];
}

interface Image {
    height: number,
    url: string,
    width: number
}

interface Artist {
    name: string;
}