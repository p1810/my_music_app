export interface ListTrack {
    albums: Item
}

interface Item {
    items: Track[]
}

interface Track {
    id: string,
    images: Image[],
    name: string,
    artists: Artist[]
}

interface Image {
    height: number,
    url: string,
    width: number
}

interface Artist {
    name: string
}
