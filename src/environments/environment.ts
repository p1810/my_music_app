// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrlSpotify: 'https://api.spotify.com',
  authorizeUrlSpotify: 'https://accounts.spotify.com',
  authorizeCodeSpotify: '/authorize',
  authorizeTokenSpotify: '/api/token',
  logoutSpotify: 'https://www.spotify.com/co/logout',
  client_id: '0c9ac63129c142f4ac0fd8816ad379af',
  client_secret: '64296643ad8e4a5f82548d6083b505c7',
  redirect_uri: 'http://localhost:4200/autorizacion',
  scope: [
    'ugc-image-upload',
    'user-read-recently-played',
    'user-top-read',
    'user-read-playback-position',
    'user-read-playback-state',
    'user-modify-playback-state',
    'user-read-currently-playing',
    'app-remote-control',
    'streaming',
    'playlist-modify-public',
    'playlist-modify-private',
    'playlist-read-private',
    'playlist-read-collaborative',
    'user-follow-modify',
    'user-follow-read',
    'user-library-modify',
    'user-library-read',
    'user-read-email',
    'user-read-private',
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
